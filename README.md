###Natural Mattress Matters
#Talalay Latex


The Talalay process is essentially the same as the Dunlop described above, but two more steps are added which changes the consistency. The rubber sap is still frothed in a centrifuge and poured into a mold. The next stages of the production cycle occur in a vacuum chamber, where the mold is then flash-frozen and baked with steam.[Natural Mattress Matters](https://www.naturalmattressmatters.com) uses talalay latex in its organic furniture line


All sediments remain suspended in place and are unable to sink to the bottom during the baking process.


The result is a more consistent cell structure throughout the mattress, from the top to the bottom, and from edge to edge. The extra steps and equipment required also mean that Talalay mattresses are a little more expensive than Dunlop products. 

